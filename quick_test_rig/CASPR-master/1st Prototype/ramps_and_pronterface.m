function ramps_and_pronterface
%Main Code: you only need to make edits on intialized variables section
close all
clc
[~, id] = lastwarn;
warning('off', id)
%% Initialized Variables

gcodeName = 'RampsTestSquare.g'; %Choose Name of gcode File, If you dont rename it will overwrite
gcodeLocation = 'C:\Users\Jack\Desktop\'; %Location of gcode File (Crucial to have \ at end)
%Gcode file location doesnt require to be in same folder as CASPR-master.

trials = traj_diagindexsquare; %Choose Trajectory to use from Paths Folder (DONT USE '')
%Other Trajectories I made are in ~/additions Jack/Trajectories/Paths
%Trajectories can be saved in any folder you make as long as its a subfolder of CASPR-master...
%...and you initialise_CASPR in the command window

tests = pos_roughmotor_2offset; %Cable Setup Configuration

winchRadius = 0.01; %average winch radius

motorStep = 1; %1 for full step, 2 for half step

fileLocation = 'C:\Users\Jack\Desktop\'; %Choose Where CASPER Folder Location is (CRUCIAL to have \ at end)
%This is necessary to find the location of the xml trajectory file to overwirte. DONT add CASPR-master\

%% XML Cables Converter

%Com vs joint and see what other options there are in the code!!!!!!!!!!

n = com.mathworks.xml.XMLUtils.createDocument('cables');
domImpl = n.getImplementation();
doctype = domImpl.createDocumentType('cables','','../../../../templates/cables.dtd');
n.appendChild(doctype);

x1 = n.getDocumentElement;
x1.setAttribute('default_cable_set','original');
    x2 = n.createElement('cable_set');
    for j = 1:1:size(tests,1)
    xa = cell2mat(tests(j,1));
    ya = cell2mat(tests(j,3));
    za = cell2mat(tests(j,5));
    xb = cell2mat(tests(j,2));
    yb = cell2mat(tests(j,4));
    zb = cell2mat(tests(j,6));
    namec = cell2mat(tests(j,7));
    num = length(xa);
    x2.setAttribute('id',namec);
        for i = 1:1:num
        x3 = n.createElement('cable_ideal');
        x3.setAttribute('name',['cable ',num2str(i)]);
        x3.setAttribute('attachment_reference','joint');
            x4 = n.createElement('properties');
                x5 = n.createElement('force_min');
                x5.appendChild(n.createTextNode(sprintf('%.1f',0)));
                x4.appendChild(x5);
                x5 = n.createElement('force_max');
                x5.appendChild(n.createTextNode(sprintf('%.1f',20)));
                x4.appendChild(x5);
            x3.appendChild(x4); %/properties
            x4 = n.createElement('attachments');
                x5 = n.createElement('attachment');
                	x6 = n.createElement('link');
                    x6.appendChild(n.createTextNode(sprintf('%i',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('location');
                    x6.appendChild(n.createTextNode(sprintf('%.4f ',[xa(j,i) ya(j,i)])));
                    x6.appendChild(n.createTextNode(sprintf('%.4f',za(j,i))));
                    x5.appendChild(x6);
                x4.appendChild(x5);
                x5 = n.createElement('attachment');
                	x6 = n.createElement('link');
                    x6.appendChild(n.createTextNode(sprintf('%i',1)));
                    x5.appendChild(x6);
                    x6 = n.createElement('location');
                    x6.appendChild(n.createTextNode(sprintf('%.4f ',[xb(j,i) yb(j,i)])));
                    x6.appendChild(n.createTextNode(sprintf('%.4f',zb(j,i))));
                    x5.appendChild(x6);  
                x4.appendChild(x5);
            x3.appendChild(x4);
        x2.appendChild(x3);
        end
    x1.appendChild(x2);
    end

xmlFileName = ...
    [[fileLocation,'CASPR-master\data\model_config\models\SCDM\spatial_manipulators\Joom_3D\Joom_3D_cables'],'.xml'];
xmlwrite(xmlFileName,n);

%% XML Trajectories Converter

n = com.mathworks.xml.XMLUtils.createDocument('trajectories');
domImpl = n.getImplementation();
doctype = domImpl.createDocumentType('trajectories','','../../../../templates/trajectories.dtd');
n.appendChild(doctype);

x1 = n.getDocumentElement;
    x2 = n.createElement('joint_trajectories');
    for j = 1:1:size(trials,1)
        x = cell2mat(trials(j,1));
        y = cell2mat(trials(j,2));
        z = cell2mat(trials(j,3));
        t = cell2mat(trials(j,4));
        namet = cell2mat(trials(j,5));
        tstep = cell2mat(trials(j,6));
        num = length(x);
        xyz_array = [x',y',z',zeros(num,2)];
        x3 = n.createElement('quintic_spline_trajectory');
        x3.setAttribute('time_definition','relative');
        x3.setAttribute('id',namet);
        x3.setAttribute('time_step',num2str(tstep));
            x4 = n.createElement('points');
                x5 = n.createElement('point');
                    x6 = n.createElement('q');
                    x6.appendChild(n.createTextNode(sprintf('%.2f ',xyz_array(1,:))));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('q_dot');
                    x6.appendChild(n.createTextNode(sprintf('%.1f ',[0 0 0 0 0])));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('q_ddot');
                    x6.appendChild(n.createTextNode(sprintf('%.1f ',[0 0 0 0 0])));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                x4.appendChild(x5);
                for i = 2:1:num
                x5 = n.createElement('point');
                x5.setAttribute('time',num2str(t(i)))
                    x6 = n.createElement('q');
                    x6.appendChild(n.createTextNode(sprintf('%.2f ',xyz_array(i,:))));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('q_dot');
                    x6.appendChild(n.createTextNode(sprintf('%.1f ',[0 0 0 0 0])));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('q_ddot');
                    x6.appendChild(n.createTextNode(sprintf('%.1f ',[0 0 0 0 0])));
                    x6.appendChild(n.createTextNode(sprintf('%.1f',0)));
                    x5.appendChild(x6);
                x4.appendChild(x5);
                end 
            x3.appendChild(x4);
        x2.appendChild(x3);
    end
    x1.appendChild(x2);
    
xmlFileName = ...
    [[fileLocation,'CASPR-master\data\model_config\models\SCDM\spatial_manipulators\Joom_3D\Joom_3D_trajectories'],'.xml'];
xmlwrite(xmlFileName,n);
%% Script to call CASPR to calculate cable lengths and cable lengths dot
%DO NOT EDIT THIS CODE

% Set up the type of model
model_config = ModelConfig('Joom 3D');
cable_set_id = namec;
trajectory_id = namet;

% Load the SystemKinematics object from the XML
modelObj = model_config.getModel(cable_set_id);

% Setup the inverse kinematics simulator with the SystemKinematics object
disp('Start Setup Simulation');
sim = InverseKinematicsSimulator(modelObj);
trajectory = model_config.getJointTrajectory(trajectory_id);
disp('Finished Setup Simulation');

% Run the kinematics on the desired trajectory
disp('Start Running Simulation');
sim.run(trajectory);
disp('Finished Running Simulation');

% After running the simulator the data can be plotted
% Refer to the simulator classes to see what can be plotted.
disp('Start Plotting Simulation');
cableLengths = sim.plotCableLengths();

clc
%% Converting Cable Lengths to Pronter Face Values

% l = cableLengths;
% l2 = [l(:,1)-l(:,2) l(:,3)-l(:,4) l(:,5)-l(:,6) l(:,7)-l(:,8)];
% for i = 1:length(cableLengths)
% fprintf('\nTower to Nozzle Diff: 12: %.4f 34: %.4f 56: %.4f 78: %.4f',l2(i,1),l2(i,2),l2(i,3),l2(i,4))
% end
% fprintf('\n\n\n\n\n\n')

pronterOrigin = cableLengths(1,:)*(40/2/pi/winchRadius);
b = pronterOrigin;
if size(cableLengths,2) == 4
fprintf('\nPronter Starting Point is: G00 X%.1f Y%.1f Z%.1f E%.1f\n',b(3),b(1),b(4),b(2))
elseif size(cableLengths,2) == 8
fprintf('\nPronter Starting Point is: %f %f %f %f %f %f %f %f\n',b(1),b(2),b(3),b(4),b(5),b(6),b(7),b(8))
else
fprintf('\nERROR, cable system is not 4 or 8')
end

if x(1) == 0 && y(1) == 0 && z(1) == 0
    fprintf('Trajectory starts at the origin\n')
else
    fprintf('Trajectory does NOT start at the origin\n')
end
if x(1) == x(length(x)) && y(1) == y(length(y)) && z(1) == z(length(z))
    fprintf('Trajectory ends at same coordinate as the beginning coordinate\n')
else
    fprintf('Trajectory does NOT end at same coordinate as the beginning coordinate\n')
end

gcodeFile = [gcodeLocation,gcodeName];

f = fopen(gcodeFile,'wt');
    for i=1:size(cableLengths,1)
        pronterFace(i,:) = cableLengths(i,:)*motorStep*40/2/pi/winchRadius;
        a = pronterFace;
        if size(cableLengths,2) == 4
        fprintf(f,'G00 X%.1f Y%.1f Z%.1f E%.1f\n',a(i,4),a(i,2),a(i,3),a(i,1));
        elseif size(cableLengths,2) == 8
        fprintf(f,'Pronter Starting Point is: %f %f %f %f %f %f %f %f\n',a(1),a(2),a(3),a(4),a(5),a(6),a(7),a(8));
        end
    end
fclose(f);

%%In case we need the addition of a pause in the system. Doesnt Work Properly Still so be careful
% for j = 2:length(t)
%     for i = 1:t(j)/tstep
%         k = (j-2)*t(j)/tstep;
%         pronterFace(i+k,:) = cableLengths(i+k,:)*motorStep*40/2/pi/winchRadius;
%         a = pronterFace;
%         fprintf('G00 X%.1f Y%.1f Z%.1f E%.1f\n',a(i+k,3),a(i+k,1),a(i+k,4),a(i+k,2))
%     end
%     if j ~= length(t) 
%     fprintf('G4 S1\n')
%     end
% end
end
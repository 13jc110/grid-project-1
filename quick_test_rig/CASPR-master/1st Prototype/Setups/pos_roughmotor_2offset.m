function c = pos_roughmotor_2offset

h = 0.25;

name = 'original';

x1 = [-0.444 -0.439 0.4358 0.4358];
x2 = [-0.022 -0.022 0.022  0.022];
y1 = [-0.4248 0.4425 0.4425 -0.4347];
y2 = [ -0.022 0.022  0.022 -0.022];
z1 = [  h      h      h      h];
z2 = [0.045 0.045 0.045 0.045];

%can provide multiple values below and specify which one you want!!!
c = {x1 x2 y1 y2 z1 z2 name};

end
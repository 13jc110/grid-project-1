function c = pos_roughnema23_2offset

h = 0.25;
h2 = 0.054;
x02 = 0.025;
y02 = 0.025;

name = 'original';

x1 = [-0.444 -0.439 0.4358 0.4358];
x2 = [-x02 -x02 x02  x02];
y1 = [-0.4248 0.4425 0.4425 -0.4347];
y2 = [ -y02 y02  y02 -y02];
z1 = [  h      h      h      h];
z2 = [h2 h2 h2 h2];

%can provide multiple values below and specify which one you want!!!
c = {x1 x2 y1 y2 z1 z2 name};

end
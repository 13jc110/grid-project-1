function c = pos_CoGiRo

name = 'original';

x1 = [-7.1775 -7.4594 -7.3911 -7.1026 7.2398 7.5208  7.4461  7.1608];
x2 = [0.5032  -0.5097 -0.5032 0.4960 -0.5032 0.4998  0.5021 -0.5045];
y1 = [-5.4361 -5.1504  5.1940  5.4753 5.3759 5.0851 -5.2539 -5.5342];
y2 = [-0.4928  0.3508 -0.2700 0.3561 0.4928 -0.3404  0.2750 -0.3463];
z1 = [5.3911  5.3999  5.3976  5.4094  5.4093 5.4200  5.3874  5.3973];
z2 = [0.0     0.9976  0.0     0.9996  0.0    0.9991 -0.0007  0.9976];

for i = 1:1:4
    valx1(i) = x1(2*(i-1)+2)-x1(2*(i-1)+1);
    valy1(i) = y1(2*(i-1)+2)-y1(2*(i-1)+1);
end
vx1 = [x1(1)  x1(4)  x1(5)  x1(8)];
vy1 = [y1(1)  y1(4)  y1(5)  y1(8)];
vx2 = [x1(2)  x1(3)  x1(6)  x1(7)];
vy2 = [y1(2)  y1(3)  y1(6)  y1(7)];
a = mean(abs(vx1)); b = std(abs(vx1))/sqrt(i);
c = mean(abs(vy1)); d = std(abs(vy1))/sqrt(i);
a2 = mean(abs(vx2)); b2 = std(abs(vx2))/sqrt(i);
c2 = mean(abs(vy2)); d2 = std(abs(vy2))/sqrt(i);
e = mean(abs(valx1)); f = std(abs(valx1))/sqrt(i);
g = mean(abs(valy1)); h = std(abs(valy1))/sqrt(i);

valx2z0 = [x2(3)+x2(5) x2(1)+x2(7)];
valy2z0 = [y2(3)-y2(5) y2(1)-y2(7)];
valx2z1 = [x2(2)+x2(8) x2(4)+x2(6)];
valy2z1 = [y2(2)-y2(8) y2(4)-y2(6)];

e2 = mean(abs(valx2z0)); f2 = std(abs(valx2z0))/sqrt(i);
g2 = mean(abs(valy2z0)); h2 = std(abs(valy2z0))/sqrt(i);
e3 = mean(abs(valx2z1)); f3 = std(abs(valx2z1))/sqrt(i);
g3 = mean(abs(valy2z1)); h3 = std(abs(valy2z1))/sqrt(i);

for i = 1:8
    l(i) = sqrt((x1(i)-x2(i))^2 + (y1(i)-y2(i))^2 + (z1(i)-z2(i))^2);
end
l2 = [l(1)-l(2) l(3)-l(4) l(5)-l(6) l(7)-l(8)];

fprintf('\n')
fprintf('\n4 Tower Average x position #1: %.4f +/- %.4f',a,b)
fprintf('\n4 Tower Average y position #1: %.4f +/- %.4f',c,d)
fprintf('\n4 Tower Average x position #2: %.4f +/- %.4f',a2,b2)
fprintf('\n4 Tower Average y position #2: %.4f +/- %.4f',c2,d2)
fprintf('\nDifference from double towers x position: %.4f +/- %.4f',e,f)
fprintf('\nDifference from double towers y position: %.4f +/- %.4f',g,h)
fprintf('\nDifference in Nozzle point x position at z = 0: %.4f +/- %.4f',e2,f2)
fprintf('\nDifference in Nozzle point y position at z = 0: %.4f +/- %.4f',g2,h2)
fprintf('\nDifference in Nozzle point x position at z = h: %.4f +/- %.4f',e3,f3)
fprintf('\nDifference in Nozzle point y position at z = h: %.4f +/- %.4f',g3,h3)
fprintf('\nTower 1 and 2 Cable Length Difference %.4f',l2(1))
fprintf('\nTower 3 and 4 Cable Length Difference %.4f',l2(2))
fprintf('\nTower 5 and 6 Cable Length Difference %.4f',l2(3))
fprintf('\nTower 7 and 8 Cable Length Difference %.4f',l2(4))
fprintf('\n')

c = {x1 x2 y1 y2 z1 z2 name};

end
function c = traj_diagsquare
%Example Template to make a trajectory, a square trajectory

name = 'trajectory '; %Name the trajectory, doesnt matter what you name it
tstep = 0.1; %Time Step from one time
l = 0.3; %Y and X axis Variables (meters)
h = 0.05; %Z axis Variable (meters)
s = 5; %Time it takes to travel to each point (seconds)
%Note travel time in code is not equivalent to real time for the system...
%...the pronter face needs to be calculated to do so

%x, y, z, and t are saved as an array below: Search arrays in MATLAB for other ways for notation
%This is an example for a full square trajectories consisting of 10 data points including...
%...starting at the origin and ending at the origin
%Point 1  2  3  4    5    6    7   8  9  10
t =   [0  s  s  s   2*s  2*s  2*s   s  s];
x =   [0  0  0  -l   0    l    0    0  0];
y =   [0  0  l  0   -l    0    l    0  0];
z =   [0  h  h  h    h    h    h    h  0];
%Note that t at point 1 needs to ALWAYS be specified as 0!!!!!!!! That's how the XML file reads Time.

%Saves all necessary outputs into cell formats
c = {x y z t name tstep};

end
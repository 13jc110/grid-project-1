function c = traj_cornertest
name = 'traj_cornertest';
tstep = 0.1;
% size(xl,1) == length(zl); SATISFY THIS!!!!!
xy = 0.2:0.02:0.35;
zint = 0.05:0.025:0.15;
z0 = 0;
in = length(xy);
jn = length(zint);
xt = xl*20;
yt = yl*20;
zt = zl*10;

t(1) = 0; x(1) = 0; y(1) = 0; z(1) = z0;
for j = 1:jn
    if j == 1
        t(2) = zt(j); x(2) = 0; y(2) = 0; z(2) = zl(j);
    else
        val = length(x);
        t(val+1) = zt(j); x(val+1) = 0;
        y(val+1) = yl(j-1,in); z(val+1) = zl(j);
    end
        for i = 1:in
            val = length(x);
            t(val+1:val+6) = [yt(j,i)  xt(j,i)  2*yt(j,i) 2*xt(j,i) 2*yt(j,i) xt(j,i)];
            x(val+1:val+6) = [0  -xl(j,i)   -xl(j,i)   xl(j,i)  xl(j,i)   0];
            y(val+1:val+6) = [yl(j,i)  yl(j,i)   -yl(j,i)  -yl(j,i)  yl(j,i)   yl(j,i)];
            z(val+1:val+6) = zl(j);
        end
end
val = length(x);
t(val+1) = yt(jn,in); x(val+1) = 0;
y(val+1) = 0; z(val+1) = zl(jn);

t(val+2) = zl(jn); x(val+2) = 0;
y(val+2) = 0; z(val+2) = z0;

c = {x y z t name tstep};

end
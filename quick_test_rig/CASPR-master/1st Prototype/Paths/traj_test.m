function c = traj_test

%Make sure this name is identical to the function name
name = 'traj_test';
tstep = 0.5;

%Sample code to make for this
xval = [-0.1:0.01:0.1];
yval = [-0.1:0.01:0.1];
zval = [0.02 0.04];
% xval = [0 1 2];
% yval = [0 2 4];
% zval = [0 5 10];
timex = 0.5;
timey = 0.5;

%For now keep ny = nx
nx = length(xval);
ny = length(yval);
nz = length(zval);
t = zeros(1,(nx*ny*nz));
x = t; y = t; z = t;

for l = 2:(nx*ny*nz)
    t(l) = timex;
end
% for m = (nx+1):nx:nx*ny*nz
%     t(m) = timey;
% end
% for m = (nx*ny+1):nx*ny:nx*ny*nz
%     t(m) = timey;
% end
    
for k = 1:nz
    for j = 1:ny
        for i = 1:nx
            x(i+nx*(j-1)+ny*nx*(k-1)) = xval(i);
            y(i+nx*(j-1)+ny*nx*(k-1)) = yval(j);
            z(i+nx*(j-1)+ny*nx*(k-1)) = zval(k);
        end
    end
end

%Leave this alone
c = {x y z t name tstep};

end
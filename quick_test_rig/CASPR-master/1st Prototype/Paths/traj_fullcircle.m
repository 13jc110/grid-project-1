function c = traj_fullcircle

name = 'traj_fullcircle';
tstep = 1;
l = 0.225;
h = 0.05;
s1 = 1;
s2 = 1;
meh = 0.05;

t(1:3) =   [0  s1  s1];
x(1:3) =   [0  0  l];
y(1:3) =   [0  0  0];
z(1:3) =   [0  h  h];

for i = 1:360*meh
    x(i+3) = l*cosd(i/meh);
    y(i+3) = l*sind(i/meh);    
    z(i+3) = h;
    t(i+3) = s2;
end

t(i+4) = s1; x(i+4) = 0; y(i+4) = 0; z(i+4) = h;
t(i+5) = s1; x(i+5) = 0; y(i+5) = 0; z(i+5) = 0;
figure(1)
c = {x y z t name tstep};

end
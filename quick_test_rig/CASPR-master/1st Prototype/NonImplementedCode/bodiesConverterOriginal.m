clc

n = com.mathworks.xml.XMLUtils.createDocument('bodies_system');
domImpl = n.getImplementation();
doctype = domImpl.createDocumentType('bodies_system','','../../../../templates/bodies.dtd');
n.appendChild(doctype);

x1 = n.getDocumentElement;
    x2 = n.createElement('links');
    x2.setAttribute('display_range','-1 1 -1 1 0 1');
    x2.setAttribute('view_angle','-37 45');
        x3 = n.createElement('link_rigid');
        x3.setAttribute('num','1');
        x3.setAttribute('name','Joom 3D');
            x4 = n.createElement('joint');
            x4.setAttribute('type','SPATIAL_EULER_XYZ');
            %x4.setAttribute('q_initial','0 0 0.4 0 0 0');
            %x4.setAttribute('q_min','-1.0 -1.0 0.0 -3.1416 -3.1416 -3.1416');
            %x4.setAttribute('q_max','1.0 1.0 .85 3.1416 3.1416 3.1416');
            x3.appendChild(x4);
            x5 = n.createElement('physical');
                x6 = n.createElement('mass');
                x6.appendChild(n.createTextNode(sprintf('%.1f',100)));
                x5.appendChild(x6); 
                x7 = n.createElement('com_location');
                x7.appendChild(n.createTextNode(sprintf('%.1f ',[0, 0, 0])));
                x5.appendChild(x7);
                x8 = n.createElement('end_location');
                x8.appendChild(n.createTextNode(sprintf('%.1f ',[0,0,0])));
                x5.appendChild(x8);   
                x9 = n.createElement('intertia');
                x9.setAttribute('ref','com');
                    I = ['Ixx','Iyy','Izz','Ixy','Ixz','Iyz'];
                    val = [1,1,1,0,0,0];
                    for i = 1:6
                        x10 = n.createElement(I(3*(i-1)+1:1:3*(i-1)+3));
                        x10.appendChild(n.createTextNode(sprintf('%.1f',val(i))));
                    x9.appendChild(x10);
                    end
                x5.appendChild(x9);
            x3.appendChild(x5);        
            x4 = n.createElement('parent');
                x5 = n.createElement('num');
                x5.appendChild(n.createTextNode(sprintf('%i',0)));
                x4.appendChild(x5);
                x5 = n.createElement('location');
                x5.appendChild(n.createTextNode(sprintf('%.1f',[0,0,0])));
                x4.appendChild(x5);
            x3.appendChild(x4);
        x2.appendChild(x3);
    x1.appendChild(x2);
    
xmlFileName = [tempname,'.xml'];
xmlwrite(xmlFileName,n);
type(xmlFileName);

clc

tests = pos_4cablerough_2offset;
%Com vs joint and see what other options there are in the code!!!!!!!!!!

n = com.mathworks.xml.XMLUtils.createDocument('cables');
domImpl = n.getImplementation();
doctype = domImpl.createDocumentType('cables','','../../../../templates/cables.dtd');
n.appendChild(doctype);

x1 = n.getDocumentElement;
x1.setAttribute('default_cable_set','original');
    x2 = n.createElement('cable_set');
    for j = 1:1:size(tests,1)
    xa = cell2mat(tests(j,1));
    ya = cell2mat(tests(j,3));
    za = cell2mat(tests(j,5));
    xb = cell2mat(tests(j,2));
    yb = cell2mat(tests(j,4));
    zb = cell2mat(tests(j,6));
    num = length(xa);
    x2.setAttribute('id','original');
        for i = 1:1:num
        x3 = n.createElement('cable_ideal');
        x3.setAttribute('name',['cable ',num2str(i)]);
        x3.setAttribute('attachment_reference','joint');
            x4 = n.createElement('properties');
                x5 = n.createElement('force_min');
                x5.appendChild(n.createTextNode(sprintf('%.1f',0)));
                x4.appendChild(x5);
                x5 = n.createElement('force_max');
                x5.appendChild(n.createTextNode(sprintf('%.1f',20)));
                x4.appendChild(x5);
            x3.appendChild(x4); %/properties
            x4 = n.createElement('attachments');
                x5 = n.createElement('attachment');
                	x6 = n.createElement('link');
                    x6.appendChild(n.createTextNode(sprintf('%i',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('location');
                    x6.appendChild(n.createTextNode(sprintf('%.4f ',[xa(j,i) ya(j,i)])));
                    x6.appendChild(n.createTextNode(sprintf('%.4f',za(j,i))));
                    x5.appendChild(x6);
                x4.appendChild(x5);
                x5 = n.createElement('attachment');
                	x6 = n.createElement('link');
                    x6.appendChild(n.createTextNode(sprintf('%i',1)));
                    x5.appendChild(x6);
                    x6 = n.createElement('location');
                    x6.appendChild(n.createTextNode(sprintf('%.4f ',[xb(j,i) yb(j,i)])));
                    x6.appendChild(n.createTextNode(sprintf('%.4f',zb(j,i))));
                    x5.appendChild(x6);  
                x4.appendChild(x5);
            x3.appendChild(x4);
        x2.appendChild(x3);
        end
    x1.appendChild(x2);
    end
    
xmlFileName = [tempname,'.xml'];
xmlwrite(xmlFileName,n);
type(xmlFileName);

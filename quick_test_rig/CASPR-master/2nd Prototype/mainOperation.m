function mainOperation
%Main Code: you only need to make edits on intialized variables section
close all
%clc
[~, id] = lastwarn;
warning('off', id)

%% Initialized Variables

gcodeName = 'Demonstration 1 Tight 0.2.g'; %Choose Name of gcode File, If you dont rename it will overwrite
%(g or tap or ngc)
%motorOrientation = 'Y1 X3 A5 Z7';
%motorOrientation = 'Z1 Y3 X4 C5 A7 B8';
motorOrientation = 'X1 Y2 Z3 A4 B5 C6 U7 V8';
codeBegin = 'F50';
codeInit = 'G1';
codeOn = '';
codeOff = '';
codeFinal = '';
codeEnd = '%%';

trials = traj_indexHouse_Tight; %Choose Trajectory to use from Paths Folder (DONT USE '')
%Other Trajectories I made are in ~/additions Jack/Trajectories/Paths
%Trajectories can be saved in any folder you make as long as its a subfolder of CASPR-master...
%...and you initialise_CASPR in the command window

tests = pos_3DPrint2; %Cable Setup Configuration

convert = -4.064; %pronterface (+40) or pokeys (-6.4) or Kflop (-4.064)
subtractOrigin = 1; %1 for yes, and 2 for no
decimal = 4; %how many decimal places for the G-code parameters

winchRadius = 0.0091; %average winch radius

fileLocation = 'C:\Users\Jack\Desktop\'; %Choose Where CASPER Folder Location is (CRUCIAL to have \ at end)
%This is necessary to find the location of the xml trajectory file to overwirte. DONT add CASPR-master\

gcodeLocation = 'C:\Users\Jack\Desktop\'; %Location of gcode File (Crucial to have \ at end)
%Gcode file location doesnt require to be in same folder as CASPR-master.

namec = 'original ';
namet = 'trajectory ';

%% XML Cable and Trajectory Converter and Script Call for cableLength Simulation

name1 = cableConverter(tests,namec,fileLocation);
[name2,time_input,tstep] = trajectoryConverter(trials,namet,fileLocation);

[cableLengths, cableAddons] = simulations(name1,name2,time_input,tstep,tests);
tallyup = cableLengths + cableAddons;

final = conversion(convert,tallyup,winchRadius,subtractOrigin);

%% Creating G-Code File

gcodeFile = [gcodeLocation,gcodeName];
comments = {codeBegin,codeInit,codeOn,codeOff,codeFinal,codeEnd};
gcodeWrite(gcodeFile,decimal,motorOrientation,final,comments,trials)

end
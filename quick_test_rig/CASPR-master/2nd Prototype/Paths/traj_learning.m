function c = traj_learning

tstep = 0.5;
s = 5;
l = 0.1;

precise_height = 0.003;
clay_tolerance = 0.007;

h = 0.006:precise_height:3*precise_height;
tol = clay_tolerance;

t = [0  s  ];
x = [0  0  ];
y = [0  l  ];
z = [0 h(1)];

for i = 1:length(h)
    val = length(x);
    x(val+1:val+6) = [l   l    -l   -l -i*tol -i*tol ];
    y(val+1:val+6) = [l l+tol l+tol  l   l       l];
    if i ~= length(h)
    z(val+1:val+6) = [h(i) h(i) h(i) h(i) h(i) h(i+1)];
    else
    z(val+1:val+6) = [h(i) h(i) h(i) h(i) h(i) h(i)];
    end
    t(val+1:val+6) = s;
    
end

plot(x,y)

c = actuatorMatrixRemake(t,x,y,z,tstep);

end
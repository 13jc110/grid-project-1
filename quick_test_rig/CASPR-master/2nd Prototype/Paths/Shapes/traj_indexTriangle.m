function c = traj_indexTriangle
tstep = 0.01;
 h0 = 0;
 h = 0.01;
 l = 0.3035;
 w = sqrt(l^2/2);
 s1 = 1;
 s2 = 1;
 jn = length(h);
 in = length(l);
 
 d = sqrt(l.^2-w.^2);
 
t(1:3) =   [0  s1(1)  s1(1)];
x(1:3) =   [0    0      0];
y(1:3) =   [0    0    l(1,1)];
z(1:3) =   [h0  h(1)  h(1)];

for j = 1:jn
    if size(l,1) ~= 1 && size(w,1) ~= 1
        n = j;
    else
        n = 1;
    end
    if j ~= 1 && jn ~= 1
        t(end+1) = s1(j-1); x(end+1) = 0;
        y(end+1) = l(n,1); z(end+1) = h(j-1);
        t(end+1) = s1(j-1); x(end+1) = 0;
        y(end+1) = l(n,1); z(end+1) = h(j);
    end
    for i = 1:in
        if i ~= 1
            t(end+1) = s1(j); x(end+1) = 0;
            y(end+1) = l(n,i); z(end+1) = h(j);
        end
        t(end+1:end+3) = [   s2      s2     s2];
        x(end+1:end+3) = [-w(n,i)  w(n,i)    0];
        y(end+1:end+3) = [-d(n,i) -d(n,i) l(n,i)];
        z(end+1:end+3) = [  h(j)    h(j)   h(j)];
    end
    plot(x,y)
end
t(end+1) = s1(j); x(end+1) = 0; y(end+1) = 0; z(end+1) = h(jn);
t(end+1) = s1(j); x(end+1) = 0; y(end+1) = 0; z(end+1) = 0;
 
w = [x' y' z']
 c = actuatorMatrixRemake(t,x,y,z,tstep);

end
function c = traj_actuatorTest

tstep = [1,2,1];
t_act = 2; % Keep this in no matter what
tstep_act = 1; %Keep this in no matter what

t = [0 1 1 0; 0 2 2 2; 0 3 0 0];
x = [0 0 1 0; 1 1 0 0; 0 0 0 0];
y = [0 0 0 0; 0 1 1 0; 0 0 0 0];
z = [0 1 1 0; 3 3 3 3; 1 0 0 0];

c = actuatorMatrixRemake(t,x,y,z,tstep,t_act,tstep_act);

% cell2mat(c(4))
% cell2mat(c(5))
% cell2mat(c(6))
end
function c = traj_indexCircle

tstep = 1;
h = 0.01;
h0 = 0;
l = 0.3;
jn = length(l);
kn = length(h);
s1 = 20;
s2 = 1;
meh = 1;

t(1:3) =   [0  s1(1)  s1(1)];
x(1:3) =   [0    0   l(1,1)];
y(1:3) =   [0    0     0];
z(1:3) =   [h0  h(1)  h(1)];

for k = 1:kn
    if size(l,1) == 1
        n = 1;
    else
        n = k;
    end
    if k ~= 1 && jn ~= 1
        t(end+1) = s1(k-1); x(end+1) = l(n,1);
        y(end+1) = 0; z(end+1) = h(k-1);
        t(end+1) = s1(k-1); x(end+1) = l(n,1);
        y(end+1) = 0; z(end+1) = h(k);
    end
    for j = 1:jn
        if j ~= 1
            t(end+1) = s1(k); x(end+1) = l(n,j);
            y(end+1) = 0; z(end+1) = h(k);
        end
        for i = 1:360*meh
            x(end+1) = l(n,j)*cosd(i/meh);
            y(end+1) = l(n,j)*sind(i/meh);    
            z(end+1) = h(k);
            t(end+1) = s2;
        end
    end
end
t(end+1) = s1(k); x(end+1) = 0; y(end+1) = 0; z(end+1) = h(kn);
t(end+1) = s1(k); x(end+1) = 0; y(end+1) = 0; z(end+1) = 0;

c = actuatorMatrixRemake(t,x,y,z,tstep);

end
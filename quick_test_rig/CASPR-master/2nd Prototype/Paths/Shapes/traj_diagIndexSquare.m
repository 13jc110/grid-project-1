function c = traj_diagIndexSquare

tstep = 0.1;
% size(xl,1) == length(zl); SATISFY THIS!!!!!
%X and Y are independant
zl = 0.15;
xl = 0.34;
yl = xl;
z0 = 0;
in = size(xl,2);
jn = size(xl,1);
xt = xl*20;
yt = yl*20;
zt = zl*20;


t(1) = 0; x(1) = 0; y(1) = 0; z(1) = z0;
for j = 1:jn
    if j == 1
        t(2) = zt(j); x(2) = 0; y(2) = 0; z(2) = zl(j);
    end
        for i = 1:in
            val = length(x);
            t(val+1:val+5) = [yt(j,i)  2*xt(j,i)  2*yt(j,i) 2*xt(j,i)  2*yt(j,i)];
            x(val+1:val+5) = [0        -xl(j,i)      0      xl(j,i)      0      ];
            y(val+1:val+5) = [yl(j,i)     0      -yl(j,i)      0        yl(j,i)];
            z(val+1:val+5) = zl(j);
        end
end
val = length(x);
t(val+1) = yt(jn,in); x(val+1) = 0;
y(val+1) = 0; z(val+1) = zl(jn);

t(val+2) = 10; x(val+2) = 0;
y(val+2) = 0; z(val+2) = z0;


c = actuatorMatrixRemake(t,x,y,z,tstep);

end
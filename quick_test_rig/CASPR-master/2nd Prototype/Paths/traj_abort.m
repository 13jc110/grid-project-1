function c = traj_abort

h = 0.05;
s = 5;
tstep = 0.5;

t = [0 s s];
x = [0 0 0];
y = [0 0 0];
z = [0 h 0];



c = actuatorMatrixRemake(t,x,y,z,tstep);

end
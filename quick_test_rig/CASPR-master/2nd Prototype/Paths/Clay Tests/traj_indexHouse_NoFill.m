function c = traj_indexHouse_NoFill

tstep = 0.1;
h = 0.006:0.006:8*0.006;
s = 5;
sw = 5;
y2 = 0.2;
y1 = 0.15;
x2 = 0.2;
x1 = 0.15;
ratio = 7;
tol = 0.05/ratio;
xd1 = -0.03; xd2 = 0.03;

u(1:3) = [ 0  0  1   ];
t(1:4) = [0 s  s   sw]; 
x(1:4) = [0 0 xd1 xd1]; 
y(1:4) = [0 0 -y2 -y2]; 
z(1:4) = [0 h(1)  h(1)   h(1) ];

for i = 1:length(h)
val = length(x);
valu = length(u);
if i ~= length(h)
    if i*tol <= (y2-y1)
    u(valu+1:valu+14) = [1   1   1     1    1     1     1    1    1     1    1    1      1             1];
    t(val+1:val+14) = [  s   s   s    s     s    s    s     s     s    s    s     s       s            s];
    x(val+1:val+14) = [ -x2 -x2  x2   x2   xd2  xd2   x1    x1   -x1  -x1  xd1   xd1     xd1          xd1 ];
    y(val+1:val+14) = [ -y2  y2  y2  -y2   -y2  -y1   -y1   y1    y1  -y1  -y1  -y2+i*tol -y2+i*tol   -y2 ];
    z(val+1:val+14) = [h(i) h(i) h(i) h(i) h(i) h(i)  h(i)  h(i) h(i) h(i) h(i)  h(i)   h(i+1)        h(i+1) ];
    else
    u(valu+1:valu+14) = [1   1   1    1     1     1    1    1     1     1    1                 1                  1         1];
    t(val+1:val+14) = [  s   s   s    s     s    s    s     s     s    s     s                 s                  s         s];
    x(val+1:val+14) = [ -x2 -x2  x2   x2   xd2  xd2   x1    x1   -x1  -x1  xd1-(i-ratio)*tol xd1-(i-ratio)*tol   xd1       xd1 ];
    y(val+1:val+14) = [ -y2  y2  y2  -y2   -y2  -y1   -y1   y1    y1  -y1    -y1              -y1                -y1       -y2 ];
    z(val+1:val+14) = [h(i) h(i) h(i) h(i) h(i) h(i)  h(i)  h(i) h(i) h(i)   h(i)             h(i+1)             h(i+1)   h(i+1) ];
    end
    figure(i)
    plot(x(val+1:val+14),y(val+1:val+14))
else
    if i*tol <= (y2-y1)
    u(valu+1:valu+14)=[ 1   1   1     1     1    1    1     1     1    1    1      0       0     0];
    t(val+1:val+14) = [  s   s   s    s     s    s    s     s     s    s    s      s       s     s];
    x(val+1:val+14) = [ -x2 -x2  x2   x2   xd2  xd2   x1    x1   -x1  -x1  xd1    xd1      0     0];
    y(val+1:val+14) = [ -y2  y2  y2  -y2   -y2  -y1   -y1   y1    y1  -y1  -y1   -y2+i*tol  0    0];
    z(val+1:val+14) = [h(i) h(i) h(i) h(i) h(i) h(i)  h(i)  h(i) h(i) h(i) h(i)   h(i)    h(i)   0];
        figure(i)
    plot(x(val+1:val+14),y(val+1:val+14))
    else
    u(valu+1:valu+13) = [1   1   1    1     1     1    1    1     1     1    0                 0        0];
    t(val+1:val+13) = [  s   s   s    s     s    s    s     s     s    s     s                 s        s];
    x(val+1:val+13) = [ -x2 -x2  x2   x2   xd2  xd2   x1    x1   -x1  -x1  xd1-(i-ratio)*tol   0        0 ];
    y(val+1:val+13) = [ -y2  y2  y2  -y2   -y2  -y1   -y1   y1    y1  -y1    -y1               0        0 ];
    z(val+1:val+13) = [h(i) h(i) h(i) h(i) h(i) h(i)  h(i)  h(i) h(i) h(i)   h(i)            h(i)       0];
        figure(i)
    plot(x(val+1:val+13),y(val+1:val+13))
    end

end

c = actuatorMatrixRemake(t,x,y,z,tstep);


end
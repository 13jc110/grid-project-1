function c = traj_indexHouse_Tight

lngth = 0.2;

int_height = 0.003;
precise_height = 0.003;
clay_tolerance = lngth/(2.5+12); %0.006896
layers = [1 1];

s = 5;
tstep = 0.5;
y2 = lngth;
y1 = y2-clay_tolerance;
x2 = lngth;
x1 = x2-clay_tolerance;
xd2 = 0.03;
xd1 = xd2+clay_tolerance;

h1 = int_height:precise_height:(layers(1)-1)*precise_height+int_height;
h2 = precise_height*layers(1)+int_height:precise_height:precise_height*(layers(2)+layers(1)-1)+int_height;

tol = clay_tolerance;
f = 0.5*tol;
len1 = 0.04;
len2 = 0.03;
trn = -y2+len2;
int = -1.5*tol;

u(1:4) = [0  0    1         1 ];
t(1:4) = [0  s    s         s ];
x(1:4) = [0  0   int       int ]; 
y(1:4) = [0  0    0        y1  ]; 
z(1:4) = [0 h1(1) h1(1)   h1(1)];

n1 = length(h1);
n2 = length(h2);
m = 30;
n = m-4;

for i = 1:n1
    val = length(x);
    x(val+1:val+m) = [ -f    -f      f      f     x1    x1   x1-len1 x1-len1 x1    x1   xd1    xd1   xd2   xd2   x2    x2    -x2   -x2   -xd2  -xd2  -xd1  -xd1  -x1   -x1  -x1+len1 -x1+len1 -x1   -x1  int-i*tol int-i*tol ];
    y(val+1:val+m) = [ y1  y1-len1 y1-len1  y1    y1    f       f     -f     -f   -y1   -y1    trn   trn   -y2   -y2   y2    y2    -y2   -y2   trn   trn   -y1   -y1   -f     -f       f       f     y1     y1        y1];
    z(val+1:val+m-1) = h1(i);
    u(val+1:val+m) = 1;
    t(val+1:val+m) = s;
    if i ~= n1
        z(val+m) = h1(i+1);
    else
        z(val+m) = h2(1);
    end
end

k = i;
j = 0;

for i = 1:n2
    val = length(x);
    if abs(int-(i+k)*tol) <= x1+0.0001
        x(val+1:val+n) = [ -f    -f      f      f     x1    x1   x1-len1 x1-len1 x1    x1    f    f     x2   x2    -x2   -x2   -f   -f    -x1   -x1  -x1+len1 -x1+len1 -x1   -x1  int-(i+k)*tol int-(i+k)*tol ];
        y(val+1:val+n) = [ y1  y1-len1 y1-len1  y1    y1    f       f     -f     -f   -y1   -y1   -y2  -y2   y2    y2    -y2   -y2  -y1   -y1   -f     -f       f       f     y1     y1        y1];
        z(val+1:val+n-1) = h2(i);
        u(val+1:val+n-1) = 1;
        t(val+1:val+n) = s;
        if i ~= n2
            z(val+n) = h2(i+1); %%%%
            u(val+n) = 1;
        else
            z(val+n) = h2(i); %%%%
            u(val+n) = 0;
        end
    else
        j = j+1;
        if i ~= length(h2)
            x(val+1:val+n) = [ -f    -f      f      f     x1    x1   x1-len1 x1-len1 x1    x1    f    f     x2   x2    -x2   -x2   -f   -f    -x1   -x1  -x1+len1 -x1+len1 -x1   -x1         -x1        -x1 ];
            y(val+1:val+n) = [ y1  y1-len1 y1-len1  y1    y1    f       f     -f     -f   -y1   -y1   -y2  -y2   y2    y2    -y2   -y2  -y1   -y1   -f     -f       f       f    y1-j*tol  y1-j*tol      y1];
            z(val+1:val+n)=[h2(i) h2(i)  h2(i)  h2(i) h2(i)  h2(i)  h2(i)  h2(i)  h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i)  h2(i)  h2(i)  h2(i)    h2(i+1)      h2(i+1)];
            u(val+1:val+n) = 1;
            t(val+1:val+n) = s;
        else
            x(val+1:val+n-2) = [ -f    -f      f      f     x1    x1   x1-len1 x1-len1 x1    x1    f    f     x2   x2    -x2   -x2   -f   -f    -x1   -x1  -x1+len1 -x1+len1 -x1   -x1      ];
            y(val+1:val+n-2) = [ y1  y1-len1 y1-len1  y1    y1    f       f     -f     -f   -y1   -y1   -y2  -y2   y2    y2    -y2   -y2  -y1   -y1   -f     -f       f       f    y1-j*tol ];
            z(val+1:val+n-2)=[h2(i) h2(i)  h2(i)  h2(i) h2(i)  h2(i)  h2(i)  h2(i)  h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i)  h2(i)  h2(i)  h2(i)      ];
            u(val+1:val+n-2) = 1;
            t(val+1:val+n-2) = s;
        end
    end

end

val = length(x);
x(val+1:val+2) = [0 0];
y(val+1:val+2) = [0 0];
z(val+1:val+2) = [h2(i) 0];
u(val+1:val+2) = [0 0];
scatter3(x,y,z,'*')
w = [x' y' z']
c = actuatorMatrixRemake(t,x,y,z,tstep);
end
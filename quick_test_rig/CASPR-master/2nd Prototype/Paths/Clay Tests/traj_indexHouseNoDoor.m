function c = traj_indexHouseNoDoor

tstep = 1;
h = 0.054:0.006:0.060;
s = 5;
y2 = 0.2;
y1 = 0.15;
x2 = 0.2;
x1 = 0.15;
tol = 0.007;
d1 = [0.03 0.03 0.03 0.03 0.03 0.017];
xd1 = -0.03; xd2 = 0.03;

% u(1:3) = [0 0  1];
% t(1:3) = [0 s  s];
% x(1:3) = [0 0 xd1]; 
% y(1:3) = [0 0 -y2]; 
% z(1:3) = [0 h(1)  h(1)];
u(1) = 0;
t(1) = 0;
x(1) = xd1;
y(1) = -y2;

n = length(h);

for i = 1:n
    val = length(x);
    u(val+1:val+6)=[  1   1   1    1     1      1   ];
    t(val+1:val+6) = [  s   s   s    s     s      s   ];
    x(val+1:val+6) = [  x2  x2 -x2  -x2 xd1-tol xd1-tol ];
    y(val+1:val+6) = [ -y2  y2  y2  -y2   -y2   -y2+tol  ];
    z(val+1:val+6) = [h(i) h(i) h(i) h(i) h(i)   h(i) ];

    for j = 1:round((x1+xd1)/d1(1))
        val = length(x);
        
        u(val+1) = 1;
        
        if mod(j,2) ~= 0
        t(val+1) = s;
        x(val+1) = xd1-j*d1(1)-tol;
        y(val+1) = -y1-tol; 
        z(val+1) = h(i);
        else
        t(val+1) = s;
        x(val+1) = xd1-j*d1(1)-tol;
        y(val+1) = -y2+tol; 
        z(val+1) = h(i);
        end
    end
    val = length(x);
    
    u(val+1:val+2) = [1 1];
    t(val+1:val+2) = [s s];
    x(val+1:val+2) = [xd1-j*d1(1)-tol -x2+tol];
    y(val+1:val+2) = [-y1   -y1];
    z(val+1:val+2) = [h(i) h(i)];
    
    for j = 1:2*y1/d1(2)
        val = length(x);
        u(val+1) = 1;
        if mod(j,2) ~= 0
            t(val+1) = s;
            x(val+1) = -x1-tol;
            y(val+1) = -y1+j*d1(2); 
            z(val+1) = h(i);
        else
            t(val+1) = s;
            x(val+1) = -x2+tol;
            y(val+1) = -y1+j*d1(2); 
            z(val+1) = h(i);
        end
    end
    
    val = length(x);
    u(val+1:val+3) = [1 1 1];
    t(val+1:val+3) = [s s s];
    x(val+1:val+3) = [-x1-tol -x1-tol -x1];
    y(val+1:val+3) = [-y1+j*d1(2)  y2-tol y2-tol];
    z(val+1:val+3) = [h(i) h(i) h(i)];
    
    for j = 1:2*x1/d1(3)
        val = length(x);
        u(val+1) = 1;
        if mod(j,2) ~= 0
            t(val+1) = s;
            x(val+1) = -x1+j*d1(3);
            y(val+1) = y1+tol; 
            z(val+1) = h(i);
        else
            t(val+1) = s;
            x(val+1) = -x1+j*d1(3);
            y(val+1) = y2-tol;
            z(val+1) = h(i);
        end
    end
    
    val = length(x);
    u(val+1:val+3) = [1 1 1];
    t(val+1:val+3) = [s s s];
    x(val+1:val+3) = [-x1+j*d1(3)+tol  -x1+j*d1(3)+tol x2-tol];
    y(val+1:val+3) = [y2-tol y1 y1];
    z(val+1:val+3) = [h(i) h(i) h(i)];
    
        for j = 1:2*y1/d1(4)
        val = length(x);
        u(val+1) = 1;
        if mod(j,2) ~= 0
            t(val+1) = s;
            x(val+1) = x1+tol;
            y(val+1) = y1-j*d1(4); 
            z(val+1) = h(i);
        else
            t(val+1) = s;
            x(val+1) = x2-tol;
            y(val+1) = y1-j*d1(4); 
            z(val+1) = h(i);
        end
        end
    
    val = length(x);
    u(val+1:val+2) = [1 1];
    t(val+1:val+2) = [s s];
    x(val+1:val+2) = [x1+tol x1+tol];
    y(val+1:val+2) = [y1-j*d1(4) -y2+tol];
    z(val+1:val+2) = [h(i) h(i)];
    
    for j = 1:round((x1+xd1)/d1(5))
        val = length(x);
        u(val+1) = 1;
        if mod(j,2) ~= 0
        t(val+1) = s;
        x(val+1) = x1-j*d1(1)+tol;
        y(val+1) = -y1-tol; 
        z(val+1) = h(i);
        else
        t(val+1) = s;
        x(val+1) = x1-j*d1(1)+tol;
        y(val+1) = -y2+tol; 
        z(val+1) = h(i);
        end
    end
    for j = 1:round(((xd2-xd1)+tol)/d1(6)-1)
        val = length(x);
        u(val+1) = 1;
        if mod(j,2) ~= 0 %ODD
        t(val+1) = s;
        x(val+1) = xd2+tol-j*d1(6);
        y(val+1) = -y1-tol; 
        z(val+1) = h(i);
        else
        t(val+1) = s;
        x(val+1) = xd2+tol-j*d1(6);
        y(val+1) = -y2+tol; 
        z(val+1) = h(i);
        end
    end
    val = length(x);
    u(val+1:val+8) = [ 1  1   1   1    1       1           1        1    ];
    t(val+1:val+8) = [ s  s   s   s    s       s           s        s    ];
    x(val+1:val+8) = [xd2+tol-j*d1(6)  x1  x1 -x1  -x1 xd2-j*d1(6) xd2-j*d1(6)   xd1  ];
    y(val+1:val+8) = [-y1 -y1  y1  y1  -y1     -y1      -y1-tol    -y2+tol];
    z(val+1:val+8) = [h(i) h(i) h(i) h(i) h(i)   h(i)      h(i)        h(i) ];   
    
    val = length(x);
    if i ~= length(h)
        u(val+1:val+2) = [1 1];
        t(val+1:val+2) = [s s];
        x(val+1:val+2) = [xd1 xd1];
        y(val+1:val+2) = [-y2+tol -y2];
        z(val+1:val+2) = [h(i+1) h(i+1)];
    else
        u(val+1:val+2) = [0 0];
        t(val+1:val+2) = [s s];
        x(val+1:val+2) = [0 0];
        y(val+1:val+2) = [0 0];
        z(val+1:val+2) = [h(i) 0];
    end
    
    
end

plot(x,y)

c = actuatorMatrixRemake(t,x,y,z,tstep);

end
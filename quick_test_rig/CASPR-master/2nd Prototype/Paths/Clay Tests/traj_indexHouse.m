function c = traj_indexHouse

tstep = 1;
h = 0.01;
s = 5;
y2 = 0.31;
y1 = 0.25;
x2 = 0.3;
x1 = 0.24;
tol = 0.016;
d1 = [0.05 0.05 0.04 0.05 0.05];
xd1 = -0.04; xd2 = 0.04;

u(1:3) = [0 0  1];
t(1:3) = [0 s  s];
x(1:3) = [0 0 xd1]; 
y(1:3) = [0 0 -y2]; 
z(1:3) = [0 h(1)  h(1)];

n = length(h);

for i = 1:n
val = length(x);
valu = length(u);
    u(valu+1:valu+13)=[  1   1   1    1     1    1    1     1     1    1    1     1        1       ];
    t(val+1:val+13) = [  s   s   s    s     s    s    s     s     s    s    s     s        s   ];
    x(val+1:val+13) = [ -x2 -x2  x2   x2   xd2  xd2   x1    x1   -x1  -x1  xd1  xd1        xd1-tol    ];
    y(val+1:val+13) = [ -y2  y2  y2  -y2   -y2  -y1   -y1   y1    y1  -y1  -y1   -y2+tol  -y2+tol    ];
    z(val+1:val+13) = [h(i) h(i) h(i) h(i) h(i) h(i)  h(i)  h(i) h(i) h(i) h(i)  h(i)     h(i)  ];

    for j = 1:round((x1+xd1)/d1(1))
        val = length(x);
        valu = length(u);
        
        u(valu+1) = 1;
        
        if mod(j,2) ~= 0
        t(val+1) = s;
        x(val+1) = xd1-j*d1(1)-tol;
        y(val+1) = -y1-tol; 
        z(val+1) = h(i);
        else
        t(val+1) = s;
        x(val+1) = xd1-j*d1(1)-tol;
        y(val+1) = -y2+tol; 
        z(val+1) = h(i);
        end
    end
    val = length(x);
    valu = length(u);
    
    u(valu+1:valu+2) = [1 1];
    t(val+1:val+2) = [s s];
    x(val+1:val+2) = [xd1-j*d1(1)-tol -x2+tol];
    y(val+1:val+2) = [-y1   -y1];
    z(val+1:val+2) = [h(i) h(i)];
    
    for j = 1:2*y1/d1(2)
        val = length(x);
        valu = length(u);
        u(valu+1) = 1;
        if mod(j,2) ~= 0
            t(val+1) = s;
            x(val+1) = -x1-tol;
            y(val+1) = -y1+j*d1(2); 
            z(val+1) = h(i);
        else
            t(val+1) = s;
            x(val+1) = -x2+tol;
            y(val+1) = -y1+j*d1(2); 
            z(val+1) = h(i);
        end
    end
    
    val = length(x);
    valu = length(u);
    u(valu+1:valu+3) = [1 1 1];
    t(val+1:val+3) = [s s s];
    x(val+1:val+3) = [-x1-tol -x1-tol -x1];
    y(val+1:val+3) = [-y1+j*d1(2)  y2-tol y2-tol];
    z(val+1:val+3) = [h(i) h(i) h(i)];
    
    for j = 1:2*x1/d1(3)
        val = length(x);
        valu = length(u);
        u(valu+1) = 1;
        if mod(j,2) ~= 0
            t(val+1) = s;
            x(val+1) = -x1+j*d1(3);
            y(val+1) = y1+tol; 
            z(val+1) = h(i);
        else
            t(val+1) = s;
            x(val+1) = -x1+j*d1(3);
            y(val+1) = y2-tol;
            z(val+1) = h(i);
        end
    end
    
    val = length(x);
    valu = length(u);
    u(valu+1:valu+3) = [1 1 1];
    t(val+1:val+3) = [s s s];
    x(val+1:val+3) = [-x1+j*d1(3)+tol  -x1+j*d1(3)+tol x2-tol];
    y(val+1:val+3) = [y2-tol y1 y1];
    z(val+1:val+3) = [h(i) h(i) h(i)];
    
        for j = 1:2*y1/d1(4)
        val = length(x);
        valu = length(u);
        u(valu+1) = 1;
        if mod(j,2) ~= 0
            t(val+1) = s;
            x(val+1) = x1+tol;
            y(val+1) = y1-j*d1(4); 
            z(val+1) = h(i);
        else
            t(val+1) = s;
            x(val+1) = x2-tol;
            y(val+1) = y1-j*d1(4); 
            z(val+1) = h(i);
        end
        end
    
    val = length(x);
    valu = length(u);
    u(valu+1:valu+2) = [1 1];
    t(val+1:val+2) = [s s];
    x(val+1:val+2) = [x1+tol x1+tol];
    y(val+1:val+2) = [y1-j*d1(4) -y2+tol];
    z(val+1:val+2) = [h(i) h(i)];
        
    for j = 1:round((x1+xd1)/d1(5))
        val = length(x);
        valu = length(u);
        if j == round((x1+xd1)/d1(5))
            u(val+1) = 0;
        else
            u(valu+1) = 1;
        end
        if mod(j,2) ~= 0
        t(val+1) = s;
        x(val+1) = x1-j*d1(1)+tol;
        y(val+1) = -y1-tol; 
        z(val+1) = h(i);
        else
        t(val+1) = s;
        x(val+1) = x1-j*d1(1)+tol;
        y(val+1) = -y2+tol; 
        z(val+1) = h(i);
        end
    end
    
    val = length(x);
    valu = length(u);
    if i ~= n
        u(valu+1:valu+2) = [1 1];
        t(val+1:val+2) = [s     s];
        x(val+1:val+2) = [x1-j*d1(5)+tol xd1];
        y(val+1:val+2) = [-y2+tol -y2];
        z(val+1:val+2) = [h(i+1) h(i+1)];
    else
        u(valu+1:valu+3) = [0 0 0];
        t(val+1:val+3) = [s   s     s];
        x(val+1:val+3) = [0   0     0];
        y(val+1:val+3) = [0   0     0];
        z(val+1:val+3) = [h(i) h(i) 0];    
    end

end

c = actuatorMatrixRemake(t,x,y,z,tstep,u);

end
function c = traj_indexHouseCalibrate2

tstep = 0.5;
h = 0.01:0.01:0.02;
s = 5;
yp = 0.15;
xp = 0.0254;
tol = 0.008;
d = 0.05;

t(1:3) = [0 s      s];    
x(1:3) = [0 0     -xp];  
y(1:3) = [0 0     -yp];   
z(1:3) = [0 h(1)   h(1)]; 


for i = 1:length(h)
    
    
if mod(i,2) ~= 0
       
val = length(x);
t(val+1:val+4) =   [  s    s     s     s     ];
x(val+1:val+4) =   [ -xp   xp    xp  -xp+tol ]; 
y(val+1:val+4) =   [ yp   yp   -yp   -yp     ];
z(val+1:val+4) =    [h(i) h(i) h(i)  h(i)    ];

    for j = 1:round((2*yp)/d)
        val = length(x);
        if mod(j,2) ~= 0
            t(val+1) = s;
            x(val+1) = xp-tol;
            y(val+1) = -yp+j*d; 
            z(val+1) = h(i);
        elseif j == round((2*yp)/d)
            t(val+1) = s;
            x(val+1) = -xp+tol;
            y(val+1) = -yp+j*d-tol; 
            z(val+1) = h(i);
        else
            t(val+1) = s;
            x(val+1) = -xp+tol;
            y(val+1) = -yp+j*d; 
            z(val+1) = h(i);
        end
    end
if i ~= length(h)
    val = length(t);
    t(val+1) = s;
    x(val+1) = -xp+tol;
    y(val+1) = -yp+j*d-tol;
    z(val+1) = h(i+1);
else
    val = length(t);
    t(val+1:val+2) = [s s];
    x(val+1:val+2) = [0 0];
    y(val+1:val+2) = [0 0];
    z(val+1:val+2) = [h(i) 0];
end





else

    for j = 1:round((2*yp)/d)
        val = length(x);
        if mod(j,2) ~= 0
            t(val+1) = s;
            x(val+1) = xp-tol;
            y(val+1) = yp-j*d; 
            z(val+1) = h(i);
        elseif j == round((2*yp)/d)
            t(val+1) = s;
            x(val+1) = -xp+tol;
            y(val+1) = yp-j*d; 
            z(val+1) = h(i);
        else
            t(val+1) = s;
            x(val+1) = -xp+tol;
            y(val+1) = yp-j*d; 
            z(val+1) = h(i);
        end
    end
    
val = length(x);
t(val+1:val+4) =   [  s    s     s     s     ];
x(val+1:val+4) =   [ xp   xp   -xp   -xp     ]; 
y(val+1:val+4) =   [ -yp  yp    yp   -yp     ];
z(val+1:val+4) =    [h(i) h(i) h(i)  h(i)    ];
    
if i ~= length(h)
    val = length(t);
    t(val+1) = s;
    x(val+1) = -xp;
    y(val+1) = -yp;
    z(val+1) = h(i+1);
else
    val = length(t);
    t(val+1:val+2) = [s s];
    x(val+1:val+2) = [0 0];
    y(val+1:val+2) = [0 0];
    z(val+1:val+2) = [h(i) 0];
end   
    
end
plot(x,y)    
    
c = actuatorMatrixRemake(t,x,y,z,tstep);

end
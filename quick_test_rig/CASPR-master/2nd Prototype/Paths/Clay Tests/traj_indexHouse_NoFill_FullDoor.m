function c = traj_indexHouse_NoFill_FullDoor
%%ONLY WORKS IF H steps are greater than RATIO!!!!!!!!!!!!!!!!!!!!!!!!!!
tstep = 0.5;
middle = 10*0.006;
h1 = 0.006:0.006:middle;
h2 = middle:0.006:middle+0.018;
s = 5;
y2 = 0.15;
y1 = 0.12;
x2 = 0.15;
x1 = 0.12;
ratio = 4;
tol = (y2-y1)/ratio;
xd1 = -0.03; xd2 = 0.03;

u(1:4) = [ 0  0  1   1];
t(1:4) = [0 s  s   s]; 
x(1:4) = [0 0  0 xd1]; 
y(1:4) = [0 0 -y2 -y2]; 
z(1:4) = [0 h1(1)  h1(1) h1(1)];

for i = 1:length(h1)
val = length(x);
valu = length(u);
if i ~= length(h1)
    if i*tol <= (y2-y1)
    u(valu+1:valu+14) = [1   1   1     1    1     1     1    1    1     1    1    1      1             1];
    t(val+1:val+14) = [  s   s   s    s     s    s    s     s     s    s    s     s       s            s];
    x(val+1:val+14) = [ -x2 -x2  x2   x2   xd2  xd2   x1    x1   -x1  -x1  xd1   xd1     xd1          xd1 ];
    y(val+1:val+14) = [ -y2  y2  y2  -y2   -y2  -y1   -y1   y1    y1  -y1  -y1  -y2+i*tol -y2+i*tol   -y2 ];
    z(val+1:val+14) = [h1(i) h1(i) h1(i) h1(i) h1(i) h1(i)  h1(i)  h1(i) h1(i) h1(i) h1(i)  h1(i)   h1(i+1)        h1(i+1) ];
    else
    u(valu+1:valu+14) = [1   1   1    1     1     1    1    1     1     1    1                 1                  1         1];
    t(val+1:val+14) = [  s   s   s    s     s    s    s     s     s    s     s                 s                  s         s];
    x(val+1:val+14) = [ -x2 -x2  x2   x2   xd2  xd2   x1    x1   -x1  -x1  xd1-(i-ratio)*tol xd1-(i-ratio)*tol   xd1       xd1 ];
    y(val+1:val+14) = [ -y2  y2  y2  -y2   -y2  -y1   -y1   y1    y1  -y1    -y1              -y1                -y1       -y2 ];
    z(val+1:val+14) = [h1(i) h1(i) h1(i) h1(i) h1(i) h1(i)  h1(i)  h1(i) h1(i) h1(i)   h1(i)             h1(i+1)  h1(i+1)   h1(i+1) ];
    end
else
    u(valu+1:valu+14) = [1   1   1    1     1     1    1    1     1     1    1                 1                  1         1];
    t(val+1:val+14) = [  s   s   s    s     s    s    s     s     s    s     s                 s                  s         s];
    x(val+1:val+14) = [ -x2 -x2  x2   x2   xd2  xd2   x1    x1   -x1  -x1  xd1-(i-ratio)*tol xd1-(i-ratio)*tol   xd1       xd1 ];
    y(val+1:val+14) = [ -y2  y2  y2  -y2   -y2  -y1   -y1   y1    y1  -y1    -y1              -y1                -y1       -y2 ];
    z(val+1:val+14) = [h1(i) h1(i) h1(i) h1(i) h1(i) h1(i)  h1(i)  h1(i) h1(i) h1(i)   h1(i)             h2(1)    h2(1)   h2(1) ];
end
end

k = i-ratio;
m = i;

for i = 1:length(h2)
    val = length(x);
    valu = length(u);
    if i ~= length(h2)
        u(valu+1:valu+14) = [1    1    1     1     1      1      1    1     1    1      1                 1                  1         1];
        t(val+1:val+14) = [  s    s    s     s     s      s     s     s     s    s      s                 s                  s         s];
        x(val+1:val+14) = [ -x2  -x2   x2    x2 xd1+tol xd1+tol x1    x1   -x1  -x1  xd1-(i+k)*tol xd1-(i+k)*tol            xd1       xd1 ];
        y(val+1:val+14) = [ -y2   y2   y2   -y2   -y2    -y1   -y1    y1    y1  -y1    -y1              -y1                -y1       -y2 ];
        z(val+1:val+14) = [h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i)   h2(i)             h2(i+1)        h2(i+1)   h2(i+1) ];
    else
        u(valu+1:valu+14) = [1    1    1     1     1      1      1    1     1    1      0               0                0         0];
        t(val+1:val+14) = [  s    s    s     s     s      s     s     s     s    s      s               s                s         s];
        x(val+1:val+14) = [ -x2  -x2   x2    x2 xd1+tol xd1+tol x1    x1   -x1  -x1  xd1-(i+k)*tol  xd1-(i+k)*tol        0         0];
        y(val+1:val+14) = [ -y2   y2   y2   -y2   -y2    -y1   -y1    y1    y1  -y1    -y1              0                0         0 ];
        z(val+1:val+14) = [h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i) h2(i)   h2(i)         h2(i)          h2(i)        0 ];

    end

end

plot(x,y)

c = actuatorMatrixRemake(t,x,y,z,tstep);


end
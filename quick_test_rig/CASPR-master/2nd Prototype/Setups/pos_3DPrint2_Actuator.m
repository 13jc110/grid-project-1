function c = pos_3DPrint2_Actuator

act = [1 1 1 1 1 1 1 1];

d = 0.0254;

%sadd = 0.004+0.004;
%h = 878+sadd;
%h = 0.915+sadd;
%h = 0.651+sadd;

padd = 0.0188+0.004;
%h = 0.617+padd;
h = 0.894+padd;

%z1 = [h h h h h h h h];
%zoffset = [0.617 0.617 0.616 0.616 0.618 0.617 0.617 0.617];
z1 = [h h h h h h h h h];

ymounts = 0.692;
xtowers = 0.899;
ypov = ymounts/2;
xpov = xtowers/2;

x12 = [-0.4283 -0.4283 -0.4289 -0.4289 0.4252 0.4252 0.4298 0.4298];
y12 = [-0.4864 -0.4294 0.4316 0.4886 0.4895 0.4325 -0.4280 -0.4850];

y1(1) = -ypov-0.131;
y1(2) = -ypov-0.0893;
y1(3) = ypov+0.0904;
y1(4) = ypov+0.1306;
y1(5) = ypov+0.1356;
y1(6) = ypov+0.093;
y1(7) = -ypov-0.093;
y1(8) = -ypov-0.1345;

x1(1) = -xpov+0.0296;
x1(2) = -xpov+0.0262;
x1(3) = -xpov+0.0239;
x1(4) = -xpov+0.0277;
x1(5) = xpov-0.03155;
x1(6) = xpov-0.0266;
x1(7) = xpov-0.0255;
x1(8) = xpov-0.0239;

h1 = 1.5*d;
h2 = 2.7475*d+1.5*d;
l = 2.7475/2*d;

x2 = [l -l -l l -l l l -l];
y2 = [-l l -l l l -l l -l];

%Regular
z2 = [h1 h2 h1 h2 h1 h2 h1 h2];

c = actuatorAddition(x1,x2,y1,y2,z1,z2);

end
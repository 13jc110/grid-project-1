function c = pos_3DPrint2
d = 0.0254;
h = 0.977;
%h = 0.878+0.008; 
val = 1.5*d;
%val = 0.135;

h1 = val;

l2 = 2.7475*d+val;
l = 2.7475/2*d;

x1 = [-0.4283 -0.4283 -0.4289 -0.4289 0.4252 0.4252 0.4298 0.4298];
y1 = [-0.4864 -0.4294 0.4316 0.4886 0.4895 0.4325 -0.4280 -0.4850];
z1 = [h h h h h h h h];

x2 = [l -l -l l -l l l -l];
y2 = [-l l -l l l -l l -l];


z2 = [h1 l2 h1 l2 h1 l2 h1 l2];

c = actuatorAddition(x1,x2,y1,y2,z1,z2);

end
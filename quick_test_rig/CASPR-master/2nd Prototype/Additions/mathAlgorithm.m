function mathAlgorithm(diag,edge,height,accuracy,loop)
%% Initial Parameters

%This just lets you know how accurate your measurements are:
n = length(edge);
edge = [edge,edge(1)];
for i = 1:2:3
    a(i) = acosd(-1*(diag(2)^2-edge(n+1-i)^2-edge(i)^2)/2/edge(n+1-1)/edge(i));
end
for i = 2:2:4
    a(i) = acosd(-1*(diag(1)^2-edge(i-1)^2-edge(i)^2)/2/edge(i)/edge(i-1));
end
calc = sum(a); %calculated angle or 360??????

val = 0;
answer = [0 0 0 0];
p = 0;

for h = loop
    for j = loop
        for k = loop
            for m = loop
                ground = [h j k m];
                ground = [ground,ground(1)];
                for i = 1:n
                    angle(2*(i-1)+1) = acosd(-1*(ground(i+1)^2-ground(i)^2-edge(i)^2)/2/ground(i)/edge(i)); %Angle12
                    angle(2*(i-1)+2) = acosd(-1*(ground(i)^2-ground(i+1)^2-edge(i)^2)/2/ground(i+1)/edge(i)); %Angle21
                end
                if abs(sum(angle)-calc) < accuracy
                    p = p + 1;
                    answer(p,:) = [h j k m];
                    val(p) = sum(angle);
                end
            end
        end
    end
end
num2 = 20;
for i = 1:size(answer,1)
    num1 = std(answer(i,:));
    if num1 < num2
        answer2 = answer(i,:);
        val2 = val(i);
        num2 = num1;
    end
end

fprintf('\nMeasured Angle is: %.4f\n',calc);
fprintf('optimal lengths are (cm): %.4f %.4f %.4f %.4f\n',answer2(1),answer2(2),answer2(3),answer2(4));
fprintf('Standard Deviation is (cm): %.4f\n',num2);
fprintf('Angle from optimal lengths is: %.4f\n',val2);
ground = answer;

%% Old Day to get ground lengths
%         c1     c2    c3    c4
% ground = [61.44 59.06 58.74 61.6];
% %ground = [61.1 61.8 61.8 61.2];
ground = [answer2,answer2(1)];
angle = 0;

for i = 1:n
    angle(2*(i-1)+1) = acosd(-1*(ground(i+1)^2-ground(i)^2-edge(i)^2)/2/ground(i)/edge(i)); %Angle12
    angle(2*(i-1)+2) = acosd(-1*(ground(i)^2-ground(i+1)^2-edge(i)^2)/2/ground(i+1)/edge(i)); %Angle21
end

%% XY Coordinates for Ground Lengths
y = zeros(1,n);
x = zeros(1,n);
%Calculating the assumption!
y(1) = -1*ground(1)*sind(angle(2*n));
x(1) = -1*ground(1)*cosd(angle(2*n));
y(4) = -1*ground(1)*sind(angle(2*n-1));
x(4) = ground(1)*cosd(angle(2*n-1));
a(1) = angle(1) + angle(2*n);

for i = 1:n-1
    a(1+i) = angle(2*(i-1)+2) + angle(2*(i-1)+3);
end 

count(1) = a(1);
xp = edge(1)*cosd(count(1));
yp = edge(1)*sind(count(1));
x(2) = x(1) + xp;
y(2) = y(1) + yp;

count(3) = 180-a(4);
xp = edge(3)*cosd(count(3));
yp = edge(3)*sind(count(3));
x(3) = x(4) + xp;
y(3) = y(4) + yp;

fprintf('\n');

fprintf('Recalculated Edge Lengths are (cm): %.4f %.4f %.4f %.4f\n',y(2)-y(1),x(3)-x(2),y(3)-y(4),x(4)-x(1))

fprintf('\n');

m = 0.0285;
m2 = [-m m -m m -m m -m m];
for i =1:2*n
fprintf('Coordinate %i (m): %.4f,%.4f,%.4f\n',i,x(floor(i/2+0.5))/100,y(floor(i/2+0.5))/100+m2(i),height(floor(i/2+0.5))/100)
end

end

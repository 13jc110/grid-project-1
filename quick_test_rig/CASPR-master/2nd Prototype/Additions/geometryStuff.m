function geometryStuff

%% Actuator math using new method

ymounts = 0.692;
xtowers = 0.899;
ypov = ymounts/2;
xpov = xtowers/2;
ysign = [-1 -1 1 1 1 1 -1 -1];
xsign = [-1 -1 -1 -1 1 1 1 1];

ypov = ypov;
xpov = xpov;

yadd = [0.131 0.0893 0.0904 0.1306 0.1356 0.093 0.093 0.1345];
xadd = [0.0296 0.0262 0.0239 0.0277 0.0316 0.0266 0.0255 0.0239];

y1 = (ypov+yadd).*ysign
x1 = (xpov-xadd).*xsign

end
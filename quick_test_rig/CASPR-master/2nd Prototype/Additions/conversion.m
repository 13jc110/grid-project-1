function final = conversion(convert,cableLengths,winchRadius,subtractOrigin)
%These are the only 2 globals in all of the files besides pokeys and mach4
for i=1:size(cableLengths,1)
    final(i,:) = (cableLengths(i,:)-subtractOrigin*cableLengths(1,:)).*convert/2/pi./winchRadius;
end
    
end
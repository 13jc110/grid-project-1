function gcodeWrite(gcodeFile,decimal,texts,a,comments,trials)
n = char(decimal+48);
m = double(texts)-48;

u = cell2mat(trials(7));
uadd = 0;
begin = cell2mat(comments(1));
int1 = cell2mat(comments(2));
int1 = [int1,' '];
on = cell2mat(comments(3));
off = cell2mat(comments(4));
int2 = cell2mat(comments(5));
fin = cell2mat(comments(6));

if length(texts) == 11
    
    for i = 1:(length(texts)+1)/3
        code = [int1,texts((i-1)*3+1),'%.',n,'f '];
        int1 = code;
    end
    code = [code,int2,'\n'];

    f = fopen(gcodeFile,'wt');
            fprintf(f, [begin,'\n']);
    fclose(f);
    f = fopen(gcodeFile,'at');
        for i=1:size(a,1)
            fprintf(f, code, a(i,m(2)),a(i,m(5)),a(i,m(8)),a(i,m(11)));
        if length(u) == length(a)
        if u(i) == 0 && u(i) ~= uadd
            fprintf(f,[off,'\n']);
            uadd = 0;
        elseif u(i) == 1 && u(i) ~= uadd
            fprintf(f,[on,'\n']);
            uadd = 1;
        end    
        end 
        end
        fprintf(f,fin);
    fclose(f);
    
elseif length(texts) == 17
    
    for i = 1:(length(texts)+1)/3
        code = [int1,texts((i-1)*3+1),'%.',n,'f '];
        int1 = code;
    end
    code = [code,int2,'\n'];
    
    f = fopen(gcodeFile,'wt');
        fprintf(f,[begin,'\n']);
    fclose(f);
    f = fopen(gcodeFile,'at');
        for i=1:size(a,1)
            fprintf(f,code,a(i,m(2)),a(i,m(5)),a(i,m(8)),a(i,m(11)),a(i,m(14)),a(i,m(17)));
        if length(u) == length(a)
        if u(i) == 0 && u(i) ~= uadd
            fprintf(f,[off,'\n']);
            uadd = 0;
        elseif u(i) == 1 && u(i) ~= uadd
            fprintf(f,[on,'\n']);
            uadd = 1;
        end    
        end 
        end
        fprintf(f,fin);
    fclose(f);
    
elseif length(texts) == 23
   
    for i = 1:(length(texts)+1)/3
        code = [int1,texts((i-1)*3+1),'%.',n,'f '];
        int1 = code;
    end
    code = [code,int2,'\n'];
    
    f = fopen(gcodeFile,'wt');
        fprintf(f,[begin,'\n']);
    fclose(f);
    f = fopen(gcodeFile,'at');
    for i=1:size(a,1)
        fprintf(f,code,a(i,m(2)),a(i,m(5)),a(i,m(8)),a(i,m(11)),a(i,m(14)),a(i,m(17)),a(i,m(20)),a(i,m(23)));
        if length(u) == length(a)
        if u(i) == 0 && u(i) ~= uadd
            fprintf(f,[off,'\n']);
            uadd = 0;
        elseif u(i) == 1 && u(i) ~= uadd
            fprintf(f,[on,'\n']);
            uadd = 1;
        end    
        end
    end
    fprintf(f,fin);
    fclose(f);
    
end
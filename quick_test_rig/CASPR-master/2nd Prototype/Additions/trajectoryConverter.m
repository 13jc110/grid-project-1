function [name,time,tstepping] = trajectoryConverter(trials,namet,fileLocation)

n = com.mathworks.xml.XMLUtils.createDocument('trajectories');
domImpl = n.getImplementation();
doctype = domImpl.createDocumentType('trajectories','','../../../../templates/trajectories.dtd');
n.appendChild(doctype);

x1 = n.getDocumentElement;
    x2 = n.createElement('joint_trajectories');
    x_all = cell2mat(trials(1));
    y_all = cell2mat(trials(2));
    z_all = cell2mat(trials(3));
    t_all = cell2mat(trials(4));
    tstepping = cell2mat(trials(5));
    lngth = cell2mat(trials(6));
    time(1) = 0;
    val1 = time(1);
    for j = 1:1:size(x_all,1)
        x = x_all(j,1:lngth(j));
        y = y_all(j,1:lngth(j));
        z = z_all(j,1:lngth(j));
        t = t_all(j,1:lngth(j));
        if length(tstepping) == 1
            tstep = tstepping;
        else
            tstep = tstepping(j);
        end
        time(j+1) = sum(t)+val1;
        val1 = time(j+1);
        xyz_array = [x',y',z',zeros(lngth(j),2)];
        x3 = n.createElement('quintic_spline_trajectory');
        x3.setAttribute('time_definition','relative');
        meh = [namet,num2str(j)];
        x3.setAttribute('id',meh);
        name(j) = {meh};
        x3.setAttribute('time_step',num2str(tstep));
            x4 = n.createElement('points');
                x5 = n.createElement('point');
                x5.setAttribute('time',num2str(t(1)))
                    x6 = n.createElement('q');
                    x6.appendChild(n.createTextNode(sprintf('%.3f ',xyz_array(1,:))));
                    x6.appendChild(n.createTextNode(sprintf('%i',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('q_dot');
                    x6.appendChild(n.createTextNode(sprintf('%i ',[0 0 0 0 0])));
                    x6.appendChild(n.createTextNode(sprintf('%i',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('q_ddot');
                    x6.appendChild(n.createTextNode(sprintf('%i ',[0 0 0 0 0])));
                    x6.appendChild(n.createTextNode(sprintf('%i',0)));
                    x5.appendChild(x6);
                x4.appendChild(x5);
                for i = 2:1:lngth(j)
                x5 = n.createElement('point');
                x5.setAttribute('time',num2str(t(i)))
                    x6 = n.createElement('q');
                    x6.appendChild(n.createTextNode(sprintf('%.3f ',xyz_array(i,:))));
                    x6.appendChild(n.createTextNode(sprintf('%i',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('q_dot');
                    x6.appendChild(n.createTextNode(sprintf('%i ',[0 0 0 0 0])));
                    x6.appendChild(n.createTextNode(sprintf('%i',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('q_ddot');
                    x6.appendChild(n.createTextNode(sprintf('%i ',[0 0 0 0 0])));
                    x6.appendChild(n.createTextNode(sprintf('%i',0)));
                    x5.appendChild(x6);
                x4.appendChild(x5);
                end 
            x3.appendChild(x4);
        x2.appendChild(x3);
    end
    x1.appendChild(x2);
    
xmlFileName = ...
    [[fileLocation,'CASPR-master\data\model_config\models\SCDM\spatial_manipulators\Joom_3D\Joom_3D_trajectories'],'.xml'];
xmlwrite(xmlFileName,n);

end
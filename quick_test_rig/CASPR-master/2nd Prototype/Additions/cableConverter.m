function name = cableConverter(tests,namec,fileLocation)

n = com.mathworks.xml.XMLUtils.createDocument('cables');
domImpl = n.getImplementation();
doctype = domImpl.createDocumentType('cables','','../../../../templates/cables.dtd');
n.appendChild(doctype);

x1 = n.getDocumentElement;
x1.setAttribute('default_cable_set',[namec,'1']);
    xa = cell2mat(tests(1));
    ya = cell2mat(tests(3));
    za = cell2mat(tests(5));
    xb = cell2mat(tests(2));
    yb = cell2mat(tests(4));
    zb = cell2mat(tests(6));
    num = length(xa);
    for j = 1:1:size(xa,1)
    x2 = n.createElement('cable_set');
    meh = [namec,num2str(j)];
    x2.setAttribute('id',meh);
    name(j) = {meh};
        for i = 1:1:num
        x3 = n.createElement('cable_ideal');
        x3.setAttribute('name',['cable ',num2str(i)]);
        x3.setAttribute('attachment_reference','joint');
            x4 = n.createElement('properties');
                x5 = n.createElement('force_min');
                x5.appendChild(n.createTextNode(sprintf('%.1f',0)));
                x4.appendChild(x5);
                x5 = n.createElement('force_max');
                x5.appendChild(n.createTextNode(sprintf('%.1f',20)));
                x4.appendChild(x5);
            x3.appendChild(x4); %/properties
            x4 = n.createElement('attachments');
                x5 = n.createElement('attachment');
                	x6 = n.createElement('link');
                    x6.appendChild(n.createTextNode(sprintf('%i',0)));
                    x5.appendChild(x6);
                    x6 = n.createElement('location');
                    x6.appendChild(n.createTextNode(sprintf('%.5f ',[xa(j,i) ya(j,i)])));
                    x6.appendChild(n.createTextNode(sprintf('%.5f',za(j,i))));
                    x5.appendChild(x6);
                x4.appendChild(x5);
                x5 = n.createElement('attachment');
                	x6 = n.createElement('link');
                    x6.appendChild(n.createTextNode(sprintf('%i',1)));
                    x5.appendChild(x6);
                    x6 = n.createElement('location');
                    x6.appendChild(n.createTextNode(sprintf('%.5f ',[xb(j,i) yb(j,i)])));
                    x6.appendChild(n.createTextNode(sprintf('%.5f',zb(j,i))));
                    x5.appendChild(x6);  
                x4.appendChild(x5);
            x3.appendChild(x4);
        x2.appendChild(x3);
        end
    x1.appendChild(x2);
    end

xmlFileName = ...
    [[fileLocation,'CASPR-master\data\model_config\models\SCDM\spatial_manipulators\Joom_3D\Joom_3D_cables'],'.xml'];
xmlwrite(xmlFileName,n);

end
function [cableLengths,cableAddons] = simulations(name1,name2,time_input,tstep,tests)
val = 0; vam = 0;
for j = 1:length(name2)

    model_config = ModelConfig('Joom 3D');
    % Set up the type of model
    cable_set_id = cell2mat(name1(floor(j/2+0.5)));
    trajectory_id = cell2mat(name2(j));

    % Load the SystemKinematics object from the XML
    modelObj = model_config.getModel(cable_set_id);

    % Setup the inverse kinematics simulator with the SystemKinematics object
    disp('Start Setup Simulation');
    sim = InverseKinematicsSimulator(modelObj);
    trajectory = model_config.getJointTrajectory(trajectory_id);
    disp('Finished Setup Simulation');

    % Run the kinematics on the desired trajectory
    disp('Start Running Simulation');
    sim.run(trajectory);
    disp('Finished Running Simulation');

    % After running the simulator the data can be plotted
    % Refer to the simulator classes to see what can be plotted.
    disp('Start Plotting Simulation');
    [val,vam] = sim.plotCableLengths(tests,time_input,tstep,j,val,vam);
    hold on
    last = size(val,1);
    for i = 1:size(val,2)
    plot(time_input(j+1),val(last,i)+vam(last,i),'o','HandleVisibility','off')
    hold on
    end
end

cableAddons = vam;
cableLengths = val;

end
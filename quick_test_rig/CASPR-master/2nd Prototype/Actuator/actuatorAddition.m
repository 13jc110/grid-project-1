function c = actuatorAddition(x1,x2,y1,y2,z1,z2,varargin)

if nargin == 6
    act = [0 0 0 0 0 0 0 0];
    motor = zeros(size(x1,2),3);
    c = {x1 x2 y1 y2 z1 z2 motor act};
elseif nargin == 8
    motor = varargin{1};
    act = varargin{2};
    c = {x1 x2 y1 y2 z1 z2 motor act};
end

end
function c = actuatorMatrixRemake(t,x,y,z,tstep,varargin)
%t_act and tstep_act

if nargin == 5
    lngth = length(t);
    u = 0;
elseif nargin == 6
        lngth = length(t);
    u = varargin{1};
    unew(1) = 2;
    val = 1;
    for i = 2:lngth
        unew(val+1:val+round(t(i)/tstep)) = 2;
        unew(val+round(t(i)/tstep)) = u(i);
        val = length(unew);
    end
    u = unew;
elseif nargin == 7
    t_act = varargin{1};
    tstep_act = varargin{2};
    n1 = size(t,1);
    n2 = size(t,2);
    if n1 ~= 1
        for j = 1:n1
            for i = 2:n2
                if t(j,i) == 0
                    lngth(j) = length(t(j,1:i))-1;
                    break
                elseif t(j,n2) ~= 0
                    lngth(j) = n2;
                end
            end
        end
        valt = zeros(2*n1-1,n2);
        valx = valt; valy = valt; valz = valt;

        for i = 1:n1
            valt(2*i-1,:) = t(i,:);
            valx(2*i-1,:) = x(i,:);
            valy(2*i-1,:) = y(i,:);
            valz(2*i-1,:) = z(i,:);
            vall(2*i-1) = lngth(i);
            valstep(2*i-1) = tstep(i);
            if i ~= n1
                valt(2*i,1) = 0;
                valt(2*i,2) = t_act;
                valx(2*i,1) = x(i,lngth(i));
                valx(2*i,2) = x(i+1,1);
                valy(2*i,1) = y(i,lngth(i));
                valy(2*i,2) = y(i+1,1);
                valz(2*i,1) = z(i,lngth(i));
                valz(2*i,2) = z(i+1,1);
                vall(2*i) = 2;
                valstep(2*i) = tstep_act;
            end
        end

        t = valt; x = valx; y = valy; z = valz;
        tstep = valstep; lngth = vall;
    end
    u = 0;
end
c = {x y z t tstep lngth u};

end
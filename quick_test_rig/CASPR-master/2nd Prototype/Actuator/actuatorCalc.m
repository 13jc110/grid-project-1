function [L2,act,actinv] = actuatorCalc(tests,time_input,tstep,j)

%Even Only
motor = cell2mat(tests(7));
act = cell2mat(tests(8));

xa = cell2mat(tests(1));
ya = cell2mat(tests(3));
za = cell2mat(tests(5));
x1 = xa(j/2,:); x2 = xa(j/2+1,:);
y1 = ya(j/2,:); y2 = ya(j/2+1,:);
z1 = za(j/2,:); z2 = za(j/2+1,:);

for i = 1:length(x1)
Li(i) = ((x1(i)-motor(i,1)).^2 + (y1(i)-motor(i,2)).^2 + (z1(i)-motor(i,3)).^2 ).^(1/2);
Lf(i) = ((x2(i)-motor(i,1)).^2 + (y2(i)-motor(i,2)).^2 + (z2(i)-motor(i,3)).^2 ).^(1/2);
end
L = Lf-Li;

tcount = tstep(j);
n = round((time_input(j+1)-time_input(j))/tstep(j));
% if L > 0
L2(1,:) = zeros(1,length(L));
    for i = 1:n
        L2(i+1,:) = tcount./(time_input(j+1)-time_input(j)).*L;
        tcount = tcount + tstep(j);
    end
% else
%     for i = 1:n
%         L2(i,:) = tcount./time_input(j).*L;
%         tcount = tcount + tstep(j);
%     end
% end
actinv = act;
for i = 1:size(act,2)
    if act(i) == 0
        act(i) = 1;
    elseif act(i) == 1
        act(i) = 0; 
    end
end

end